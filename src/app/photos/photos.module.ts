import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoViewComponent } from './photo-view/photo-view.component';
import { PhotoCardComponent } from './photo-card/photo-card.component';

@NgModule({
  declarations: [PhotoViewComponent, PhotoCardComponent],
  imports: [CommonModule],
  providers: [],
})
export class PhotosModule {}
