import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../photos.service';

@Component({
  selector: 'app-photo-view',
  styleUrls: ['./photo-view.component.scss'],
  templateUrl: './photo-view.component.html',
})
export class PhotoViewComponent implements OnInit {
  private photos: any[] = [];

  constructor(private service: PhotosService) {}

  public ngOnInit(): void {
    this.service.getPhotos().subscribe(
      (res: any) => {
        this.photos = res;
      },
      (err: Error) => {
        console.error(err);
      },
    );
  }

  public getPhotos(): any[] {
    return this.photos.slice(0, 12);
  }
}
