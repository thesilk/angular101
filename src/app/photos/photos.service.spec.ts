import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { PhotosService } from './photos.service';

const mockPhotos = [
  { title: '1', thumbnailUrl: '1' },
  { title: '2', thumbnailUrl: '2' },
];

describe('PhotosService', () => {
  let service: PhotosService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(PhotosService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return photos', () => {
    service.getPhotos().subscribe((photos: any[]) => {
      expect(photos).not.toBe(null);
      expect(JSON.stringify(photos)).toEqual(JSON.stringify(mockPhotos));
    });

    const req = httpTestingController.expectOne(
      `https://jsonplaceholder.typicode.com/photos`,
    );

    req.flush(mockPhotos);
  });
});
