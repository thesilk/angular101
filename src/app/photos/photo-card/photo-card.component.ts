import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-photo-card',
  templateUrl: './photo-card.component.html',
  styleUrls: ['./photo-card.component.scss']
})
export class PhotoCardComponent implements OnInit {
  @Input() public photoElement: any = {thumbnailUrl: '1', title: '1'};

  constructor() { }

  ngOnInit(): void {
  }

}
