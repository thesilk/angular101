import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {PhotosModule} from './photos.module';

@Injectable({
  providedIn: 'root',
})
export class PhotosService {

  constructor(private http: HttpClient) { }

  public getPhotos(): Observable<any> {
    return this.http.get<any>('https://jsonplaceholder.typicode.com/photos');
  }
}
