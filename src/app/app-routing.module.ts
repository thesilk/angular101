import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeViewComponent} from './home/home-view/home-view.component';
import {PhotoViewComponent} from './photos/photo-view/photo-view.component';

const routes: Routes = [
  { path: '', component: HomeViewComponent },
  { path: 'photos', component: PhotoViewComponent },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})
export class AppRoutingModule {}
